from django.contrib import admin

from django.urls import path
from django.urls import include

from django.conf.urls.static import static
from django.conf import settings

from . import views

from products.views import ProductListView

urlpatterns = [
    path('', ProductListView.as_view(), name='index'),
    #path('categories/<int:categoria_id>',views.producto_categoria,name="categoria"),
    path('acerca_de/', views.acerca_de, name='acerca_de'),
    path('usuarios/login', views.login_view, name='login'),
    path('usuarios/logout', views.logout_view, name='logout'),
    path('usuarios/registro', views.register, name='register'),
    path('admin/', admin.site.urls),
    path('nuevo-producto/', views.newproducts, name='newproducts'),
    path('productos/', include('products.urls')), #products/:id
    path('categorias/', include('categories.urls')),
    path('carrito/', include('carts.urls')),
    path('orden/', include('orders.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)