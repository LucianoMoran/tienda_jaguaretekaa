from django.urls import path
from django.urls.resolvers import URLPattern

from . import views

from .views import CategoriesListView

app_name = 'categories'

urlpatterns = [
    path('', CategoriesListView.as_view(), name='categorias')
]