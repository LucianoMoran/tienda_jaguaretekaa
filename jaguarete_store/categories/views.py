from django.shortcuts import render

from django.views.generic.list import ListView

from products.models import Product
from categories.models import Category



class CategoriesListView(ListView):
    template_name = 'Categories/categoria.html'
    queryset = Category.objects.all().order_by('-id')


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        context['categories'] = context['category_list']
        return context



# def categories(request):
#     categoria_id = Category.objects.get(id)
#     categoria = Category.objects.get(pk=categoria_id)
#     categorias = Category.objects.all().order_by('-id')
#     productos = Product.objects.all().filter(categoria=categoria)
#     titulo = categoria.descripcion

#     return render(request, 'Categories/categoria.html', {
#         'categoria':categoria,
#         'categorias':categorias,
#         'productos': productos,
#         'titulo':titulo
#     })
